<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_m extends CI_Model {

	function __construct()
    {
   		parent::__construct();

    }

    public function get_free_active_users()
	{

		return $this->db->where('ProStatus',0)->where('ActivityStatus',1)->get('users')->result_array();

	}
	

	public function get_free_in_active_users()
	{
		return $this->db->where('ProStatus',0)->where('ActivityStatus',0)->get('users')->result_array();

	}

	public function get_pro_active_users()
	{
		return $this->db->where('ProStatus',1)->where('ActivityStatus',1)->get('users')->result_array();

	}

	public function get_pro_in_active_users()
	{
		return $this->db->where('ProStatus',0)->where('ActivityStatus',0)->get('users')->result_array();

	}


}?>
