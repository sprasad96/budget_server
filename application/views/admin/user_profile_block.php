<div class="info pb-lg-3">
    <div class="img pt-6 pb-3 mx-auto d-table">
        <img src="<?php echo $user_details[0]['ProfileImageUrl']; ?>" alt="image description">
    </div>
    <span class="mb-9 d-block text-center font-weight-semi"><?php echo $user_details[0]['FullName']; ?></span>

    <ul class="member-info list-unstyled">
        <li class="mb-5 mb-lg-6">
            <h6 class="font-weight-medium">Email: <span class="font-weight-normal"><?php echo $user_details[0]['EmailId']; ?></span></h6>
        </li>

        <li class="mb-5 mb-lg-6">
            <h6 class="font-weight-medium">Employment-type: <span class="font-weight-normal"><?php echo $user_details[0]['EmployementType']; ?></span></h6>
        </li>

        <li class="mb-5 mb-lg-6">
            <h6 class="font-weight-medium">Country: <span class="font-weight-normal"><?php echo $user_details[0]['Country']; ?></span></h6>
        </li>

        <li class="mb-5 mb-lg-6">
            <h6 class="font-weight-medium">Joined On: <span class="font-weight-normal"><?php echo $user_details[0]['JoiningDate']; ?></span></h6>
        </li>

        <li class="mb-5 mb-lg-6">
            <h6 class="font-weight-medium">Membership: <span class="font-weight-normal"><?php if($user_details[0]['ProStatus'] == 1){ echo "Pro";}else{ echo "Free"; } ?></span></h6>
        </li>

    </ul>
</div>