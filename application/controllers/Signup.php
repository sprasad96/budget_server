<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
    }
    

    public function registerUser()
    {

		if($this->db->where('EmailId',$_POST['email'])->get('users')->num_rows() > 0)
		{
			$res_data['id'] = 0;
			$res_data['token'] = 0;
			$res_data['message'] = 'Already registerd Please SignIn';

		}else{

		
		$data['FullName'] = $_POST['name'];
		$data['EmailId'] = $_POST['email'];
		$data['Password'] = md5($_POST['password']);
		$data['EmployementType'] = $_POST['employment'];
		$data['Country'] = $_POST['country'];
		$data['JoiningDate'] = date('j F Y');
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$randomString = '';
		for ($i = 0; $i < 10; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		$data['Token'] = $randomString;
		                    
		if($this->db->insert('users',$data))
		{
			$id = $this->db->insert_id();
			$this->send_activation_mail($id,$randomString,$data['EmailId']);

				$res_data['id'] = $id;
				$res_data['token'] = $randomString;
				$res_data['email'] = $data['EmailId'];
			
		}else{
			$res_data['id'] = 0;
			$res_data['token'] = 0;
			$res_data['message'] = '';
		}
	}

		echo json_encode($res_data);
		

	
	}
	

	
	public function send_activation_mail($id,$token,$email)
	{

		echo $id."</br>";

		echo $token."</br>";
		echo $email."</br>";

		$headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$url = base_url()."Signup/ActivateUser/".$id."/".$token;
		//$to = $this->input->post('email');
		$message = "<!DOCTYPE html>
	<html>
	<body>

	<table>
	  <tr>
	    <th>Activation Link</th>
	    <th>".$url."</th>

	  </tr>


	</table>

	</body>
	</html>
	";
		//$to = "sprasad96@gmail.com";
		$to = $email;
		$from = "support@moniwyse.com";
		$subject = "Account Activation";
		$message = $message;
		$this -> load -> library('email');
		$this -> email -> set_newline("\r\n");
		$this->email->set_mailtype("html");
		$this -> email -> from($from, 'Support MOniwyse');
		$this -> email -> to($to);
		$this -> email -> subject($subject);
		$this -> email -> message($message);

		
		if($this->email->send()){
			return 1;
		}else{
			return 0;
		}
	}



   public function sendVerifyMail()
   {
	   $id = $_POST['id'];
	   $token = $_POST['token'];
	   $email = $_POST['email_id'];

	   $result = $this->send_activation_mail($id,$token,$email);
	   if($result)
	   {
		   echo 1;
	   }else{
		   echo 0;
	   }
   }

   public function ActivateUser($id,$token)
   {
	   $data['VerifyStatus'] = 1;

	   if($this->db->where('Id',$id)->where('Token',$token)->update('users',$data))
	   {
		   redirect('/welcome');
	   }else{
			redirect('/welcome');
	   }

   }

   public function forgotPassword()
   {
	    $email = $_POST['email'];
		$result = $this->db->where('EmailId',$email)->get('users')->result_array();
		if($result)
	   {
		   echo $result[0]['Id'];
	   }else{
			echo 0;
	   }
   }

   public function resetPassword()
   {
	$user_id = $_POST['id'];
	$password = $_POST['password'];
	$data['Password'] = md5($password);
	if($this->db->where('Id',$user_id)->update('users',$data))
	{
		echo 1;
	}else{
		echo 0;
	}
   }

    
	
}
