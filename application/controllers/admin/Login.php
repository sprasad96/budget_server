<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/login');
    }
    

    public function authenticate()
    {

        $data['user_email'] = $_POST['user_email'];
        $data['password'] = md5($_POST['password']);
                    

		$user_details = $this->alogin_m->login_authentication($data);
		
	

		if($user_details)
		{

		


			foreach($user_details as $auth_user)
			{
		$session_data = array(	'admin_user_id'  =>$auth_user['Id'],
								'admin_full_name'  => $auth_user['FullName'],
								'admin_email_id'  => $auth_user['EmailId'],
								'admin_profile_image'  => $auth_user['ProfileImageUrl'],
								'admin_role'  => $auth_user['Role'],
								'logged_in' => TRUE
						);
					}

		$this->session->set_userdata($session_data);
	//	redirect('dashboard');
		$res_data['result'] = 1;
		$res_data['key'] = 'success';

		
		}else{

	
			$res_data['result'] = 0;
			$res_data['key'] = 'login_fail';
		}

		echo json_encode($res_data);
    }

   

    
	
}
