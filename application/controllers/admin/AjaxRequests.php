<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AjaxRequests extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

     
	function __construct() {
        parent::__construct();
		$is_logged =  $this->session->userdata('admin_user_id');
		if(empty($is_logged)){
		 redirect(base_url('admin'));
		}
    }
    
	public function index()
	{
		
	}


	public function getUserDetails()
	{
		$id = $_POST['id'];
		$data['user_details'] = $this->db->where('Id',$id)->get('users')->result_array();
		echo $this->load->view('admin/user_profile_block',$data,TRUE);
	}
}
